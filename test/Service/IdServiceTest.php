<?php

namespace CommonTest\Service;


use Common\Exception\LogicException;
use Common\Service\IdService;

class IdServiceTest extends \PHPUnit_Framework_TestCase
{

    public function testGettingCorrectUriWithQNameWithTunDefault()
    {
        IdService::setDefaultQNamePrefix('tun');
        $this->assertEquals('http://id.luomus.fi/GV.1', IdService::getUri('GV.1'));
        $this->assertEquals('http://id.luomus.fi/GV.tun:E.1', IdService::getUri('GV.tun:E.1'));
        $this->assertEquals('http://tun.fi/GV.1', IdService::getUri('tun:GV.1'));
        $this->assertEquals('http://tun.fi/http.1', IdService::getUri('tun:http.1'));
        $this->assertEquals('http://id.luomus.fi/http.1', IdService::getUri('luomus:http.1'));
        $this->assertEquals('http://tun.fi/http.1', IdService::getUri('http.1', true));
        $this->assertEquals('http://id.luomus.fi/http.1', IdService::getUri('http.1'));
    }

    public function testGettingCorrectUriWithQNameWithLuomusDefault()
    {
        IdService::setDefaultQNamePrefix('luomus');
        $this->assertEquals('http://id.luomus.fi/GV.1', IdService::getUri('GV.1'));
        $this->assertEquals('http://id.luomus.fi/GV.tun:E.1', IdService::getUri('GV.tun:E.1'));
        $this->assertEquals('http://tun.fi/GV.1', IdService::getUri('tun:GV.1'));
        $this->assertEquals('http://tun.fi/http.1', IdService::getUri('tun:http.1'));
        $this->assertEquals('http://id.luomus.fi/http.1', IdService::getUri('luomus:http.1'));
        $this->assertEquals('http://id.luomus.fi/http.1', IdService::getUri('http.1', true));
        $this->assertEquals('http://id.luomus.fi/http.1', IdService::getUri('http.1'));
    }

    public function testGettingCorrectUriWithUri()
    {
        $this->assertEquals('http://id.luomus.fi/GV.1', IdService::getUri('http://id.luomus.fi/GV.1'));
        $this->assertEquals('http://tun.fi/GV.1', IdService::getUri('http://tun.fi/GV.1'));
    }

    public function testGettingQnameWithUri()
    {
        $this->assertEquals('luomus:GV.1', IdService::getQName('http://id.luomus.fi/GV.1'));
        $this->assertEquals('tun:GV.1', IdService::getQName('http://tun.fi/GV.1'));
        $this->assertEquals('luomus:GV.tun:E.1', IdService::getQName('http://id.luomus.fi/GV.tun:E.1'));
    }

    public function testGettingQNameWithQName()
    {
        $this->assertEquals('herbo:1', IdService::getQName('1'));
        $this->assertEquals('luomus:GV.1', IdService::getQName('GV.1'));
        $this->assertEquals('luomus:GV.1', IdService::getQName('luomus:GV.1'));
        $this->assertEquals('tun:GV.1', IdService::getQName('tun:GV.1'));
        $this->assertEquals('luomus:http.1', IdService::getQName('http.1'));
        $this->assertEquals('luomus:http.1', IdService::getQName('luomus:http.1'));
        $this->assertEquals('rdf:_1', IdService::getQName('rdf:_1'));
    }

    public function testGettingQNameWithDbStyle()
    {
        $this->assertEquals('GV.1', IdService::getQName('GV.1', true));
        $this->assertEquals('GV.1', IdService::getQName('luomus:GV.1', true));
        $this->assertEquals('tun:GV.1', IdService::getQName('tun:GV.1', true));
        $this->assertEquals('http.1', IdService::getQName('http.1', true));
        $this->assertEquals('http.1', IdService::getQName('luomus:http.1', true));
        $this->assertEquals('rdf:_1', IdService::getQName('rdf:_1', true));
        $this->assertEquals('herbo:1', IdService::getQName('1', true));
    }

    public function testQNamePrefixCheck()
    {
        $this->assertTrue(IdService::hasQNamePrefix('luomus:'));
        $this->assertTrue(IdService::hasQNamePrefix('herbo'));
        $this->assertFalse(IdService::hasQNamePrefix('jokin'));
        $this->assertFalse(IdService::hasQNamePrefix('toinen:'));
    }

    public function testGettingQNameWithoutPrefix()
    {
        $this->assertEquals('GV.1', IdService::getQNameWithoutPrefix('luomus:GV.1'));
        $this->assertEquals('GV.1', IdService::getQNameWithoutPrefix('GV.1'));
        $this->assertEquals('GV.1', IdService::getQNameWithoutPrefix('http://id.luomus.fi/GV.1'));
        $this->assertEquals('1', IdService::getQNameWithoutPrefix('herbo:1'));
        $this->assertEquals('1', IdService::getQNameWithoutPrefix('http://id.herb.oulu.fi/1'));
    }

    public function testGettingQNameWithArray()
    {
        $data = [
            'GV.1',
            'luomus:GV.1',
            'tun:GV.1',
            'http://id.herb.oulu.fi/1123',
            'http://id.zmuo.oulu.fi/GV.1'
        ];
        $this->assertEquals([
            'luomus:GV.1',
            'luomus:GV.1',
            'tun:GV.1',
            'herbo:1123',
            'zmuo:GV.1',
        ],
            IdService::getQName($data)
        );
        $this->assertEquals([
            'GV.1',
            'GV.1',
            'tun:GV.1',
            'herbo:1123',
            'zmuo:GV.1',
        ],
            IdService::getQName($data, true)
        );
    }

    public function testGettingUriWithArray()
    {
        $data = [
            'http://id.luomus.fi/GV.1',
            'GV.1',
            'luomus:GV.1',
            'tun:GV.1',
            'herbo:1123',
            'zmuo:GV.1',
        ];
        $this->assertEquals([
            'http://id.luomus.fi/GV.1',
            'http://id.luomus.fi/GV.1',
            'http://id.luomus.fi/GV.1',
            'http://tun.fi/GV.1',
            'http://id.herb.oulu.fi/1123',
            'http://id.zmuo.oulu.fi/GV.1'
        ],
            IdService::getUri($data, true)
        );

    }

    public function testGettingUriParts()
    {
        $this->assertEquals(['http://id.luomus.fi/', 'GV.1'], IdService::getUriInParts('http://id.luomus.fi/GV.1'));
        $this->assertEquals(['http://tun.fi/', 'GV.1'], IdService::getUriInParts('http://tun.fi/GV.1'));
        $this->assertEquals(['http://id.luomus.fi/', 'GV.1'], IdService::getUriInParts('GV.1'));
        $this->assertEquals(['http://tun.fi/', 'GV.1'], IdService::getUriInParts('tun:GV.1'));
        $this->assertEquals(['http://id.luomus.fi/', 'foo.laa'], IdService::getUriInParts('foo.laa'));
        $this->assertEquals(['http://id.herb.oulu.fi/', 'laa'], IdService::getUriInParts('laa'));
        $this->assertEquals(['http://id.luomus.fi/', 'jokin:laa'], IdService::getUriInParts('jokin:laa'));
        $this->assertEquals(['http://id.herb.oulu.fi/', 'laa'], IdService::getUriInParts(IdService::getQName('laa')));
    }

    public function testGeneratedIdWithDefaultDomain()
    {
        IdService::setDefaultQNamePrefix('tun');
        $idService = $this->getGeneratedIdService('HT', 123);
        $this->assertEquals('tun:HT.123', $idService->getPQDN());
        $this->assertEquals('http://tun.fi/HT.123', $idService->getFQDN());
    }

    public function testGeneratedIdWithTunDomain()
    {
        $idService = $this->getGeneratedIdService('tun:MOS', '123');
        $this->assertEquals('tun:MOS.123', $idService->getPQDN());
        $this->assertEquals('http://tun.fi/MOS.123', $idService->getFQDN());
    }

    protected function getGeneratedIdService($ns, $id)
    {
        $idService = $this->getIdService($ns, $id);
        $idService->generate($ns);

        return $idService;
    }

    protected function getIdService($ns, $id)
    {
        $genMock = $this->getMockBuilder('Common\Db\SequenceGeneratorInterface')->getMock();
        $onlyNs = $ns;
        $pos = strpos($ns, ':');
        if ($pos !== false) {
            $onlyNs = substr($ns, $pos + 1);
        }
        $genMock->expects($this->once())
            ->method('getNextSeqVal')
            ->with($this->equalTo($onlyNs))
            ->will($this->returnValue($id));

        return new IdService($genMock);
    }

    public function testValidQNamePrefix()
    {
        $this->assertFalse(IdService::isValidDefaultQNamePrefix('none'));
        $this->assertFalse(IdService::isValidDefaultQNamePrefix(''));
        $this->assertTrue(IdService::isValidDefaultQNamePrefix('luomus'));
        $this->assertTrue(IdService::isValidDefaultQNamePrefix('herbo'));
    }

    public function testNSRegExp()
    {
        $this->assertEquals(1, preg_match(IdService::NS_REGEXP, 'luomus:GV', $match));
        $this->assertEquals('luomus:', $match[IdService::KEY_DOMAIN_PREFIX]);
        $this->assertEquals('GV', $match[IdService::KEY_NS]);

        $this->assertEquals(1, preg_match(IdService::NS_REGEXP, 'E', $match));
        $this->assertEquals('', $match[IdService::KEY_DOMAIN_PREFIX]);
        $this->assertEquals('E', $match[IdService::KEY_NS]);

        $this->assertRegExp(IdService::NS_REGEXP, 'kli');
        $this->assertRegExp(IdService::NS_REGEXP, '3li');
        $this->assertRegExp(IdService::NS_REGEXP, 'tun:GV');
        $this->assertNotRegExp(IdService::NS_REGEXP, 'none:GV');
        $this->assertNotRegExp(IdService::NS_REGEXP, 'GV-1');
        $this->assertNotRegExp(IdService::NS_REGEXP, '(GV)');
        $this->assertNotRegExp(IdService::NS_REGEXP, 'G E');
        $this->assertNotRegExp(IdService::NS_REGEXP, 'GVEORT');
        $this->assertNotRegExp(IdService::NS_REGEXP, '');
    }

    public function testIDRegExp()
    {
        $this->assertRegExp(IdService::ID_REGEXP,'GV.123');
        $this->assertRegExp(IdService::ID_REGEXP,'herbo:123');
        $this->assertRegExp(IdService::ID_REGEXP,'luomus:GV.123');
        $this->assertNotRegExp(IdService::ID_REGEXP,'luomus:123');
        $this->assertNotRegExp(IdService::ID_REGEXP,'luomus:H.H1-123');
        $this->assertNotRegExp(IdService::ID_REGEXP,'');

        $this->assertEquals(1, preg_match(IdService::ID_REGEXP, 'tun:EA4.A1234G2', $match));
        $this->assertEquals('tun:', $match[IdService::KEY_DOMAIN_PREFIX]);
        $this->assertEquals('EA4', $match[IdService::KEY_NS]);
        $this->assertEquals('A1234G2', $match[IdService::KEY_OID]);

        $this->assertEquals(1, preg_match(IdService::ID_REGEXP, 'herbo:12342', $match));
        $this->assertEquals('herbo:', $match[IdService::KEY_DOMAIN_PREFIX]);
        $this->assertEquals('', $match[IdService::KEY_NS]);
        $this->assertEquals('12342', $match[IdService::KEY_OID]);

        $this->assertEquals(1, preg_match(IdService::ID_REGEXP, 'luomus:H.H-12342', $match));
        $this->assertEquals('luomus:', $match[IdService::KEY_DOMAIN_PREFIX]);
        $this->assertEquals('H', $match[IdService::KEY_NS]);
        $this->assertEquals('H-12342', $match[IdService::KEY_OID]);
    }

    public function testDomainRegExp()
    {
        $this->assertNotRegExp(IdService::URI_REGEXP, 'http://google.com/GV.1');
        $this->assertNotRegExp(IdService::URI_REGEXP, 'http://id.zmuo.oulu.fi/1234G2');

        $this->assertEquals(1, preg_match(IdService::URI_REGEXP, 'http://id.zmuo.oulu.fi/EA4.A1234G2', $match));
        $this->assertEquals('id.zmuo.oulu.fi', $match[IdService::KEY_DOMAIN]);
        $this->assertEquals('EA4', $match[IdService::KEY_NS]);
        $this->assertEquals('A1234G2', $match[IdService::KEY_OID]);

        $this->assertEquals(1, preg_match(IdService::URI_REGEXP, 'http://id.herb.oulu.fi/123213213', $match));
        $this->assertEquals('id.herb.oulu.fi', $match[IdService::KEY_DOMAIN]);
        $this->assertEquals('', $match[IdService::KEY_NS]);
        $this->assertEquals('123213213', $match[IdService::KEY_OID]);
    }

    public function testPartGenerator()
    {
        $generator = new IdService();
        $this->assertEquals('herbo:1', IdService::getQName($generator->generateFromPieces(1,'herbo:', 'zoo')->getFQDN()));
        $this->assertEquals('herbo:1', IdService::getQName($generator->getFQDN()));
        $this->assertEquals('herbo:1', IdService::getQName($generator->generateFromPieces(1,'', 'zoo')->getFQDN()));
        $this->assertEquals('tun:GV.1', IdService::getQName($generator->generateFromPieces(1,'GV', 'zoo')->getFQDN()));
        IdService::setDefaultQNamePrefix('luomus');
        $this->assertEquals('luomus:GV.1', IdService::getQName($generator->generateFromPieces(1,'GV', 'zoo')->getFQDN()));
    }

    /**
     * @expectedException        LogicException
     * @expectedExceptionMessage You have to first generate the id
     */
    public function testClearingGenerator()
    {
        $generator = new IdService();
        $this->assertEquals('herbo:1', IdService::getQName($generator->generateFromPieces(1,'', 'zoo')->getFQDN()));
        $this->assertEquals('herbo:1', IdService::getQName($generator->getFQDN()));
        $generator->clear();
        $this->assertNotEquals('herbo:1', IdService::getQName($generator->getFQDN()));
    }

}