<?php

namespace Common\Service;


class NameService
{

    /**
     * Generates the organization name from organization document.
     *
     * @param $organization
     * @param bool $addAbbr
     * @return String
     */
    public function getOrganizationName($organization, $addAbbr = true)
    {
        if ($organization === null) {
            return '';
        }
        $name = array();
        $fields = array(
            'getMOSOrganizationLevel4',
            'getMOSOrganizationLevel3',
            'getMOSOrganizationLevel2',
            'getMOSOrganizationLevel1');

        foreach ($fields as $method) {
            if (!method_exists($organization, $method)) {
                continue;
            }
            $value = trim($organization->$method('en'));
            if ($value !== '' && $value !== null) {
                $name[] = $value;
            }
        }
        $name = join(', ', $name);
        if ($addAbbr && method_exists($organization, 'getMOSAbbreviation')) {
            $abbreviation = $organization->getMOSAbbreviation();
            if ($abbreviation !== '' && $abbreviation !== null) {
                $name = $abbreviation . ' - ' . $name;
            }
        }
        return $name;
    }

}