<?php
namespace Common\Db;

/**
 * Interface SequenceGeneratorInterface is used to generate all the object id's
 *
 * @package Common\Db
 */
interface SequenceGeneratorInterface {

    /**
     * Gets the next sequence number for the given namespace
     *
     * @param string $namespace
     *
     * @return string
     */
    public function getNextSeqVal($namespace);

} 